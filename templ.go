package templ

import (
	"bytes"
	"fmt"
	"io"
	"reflect"
	"strings"

	"gitlab.com/metakeule/places"
)

type Setter interface {
	io.WriterTo
	Name() string
}

// a named buffer, fullfills the Setter interface
type Buffer struct {
	*bytes.Buffer
	name string
}

var _ Setter = &Buffer{}

func newBuffer(name string) *Buffer {
	bf := &Buffer{}
	bf.name = name
	bf.Buffer = &bytes.Buffer{}
	return bf
}

func (b *Buffer) Name() string {
	return b.name
}

type Escaper map[string]func(interface{}) string

type View struct {
	_type      string
	_tag       string
	_positions map[string]Position
}

func (esc Escaper) View(stru interface{}, tag string) *View {
	s := &View{_type: structName(stru), _tag: tag}
	s.scanPositions(stru, esc)
	return s
}

func structName(stru interface{}) string {
	return strings.Replace(fmt.Sprintf("%T", stru), "*", "", 1)
}

func (str *View) Tag() string  { return str._tag }
func (str *View) Type() string { return str._type }

func (str *View) Position(field string) Position {
	p, ok := str._positions[field]
	if !ok {
		panic(fmt.Sprintf("no position for field %s in struct %s (tag: %s)", field, str._type, str._tag))
	}
	return p
}

func (str *View) HasPosition(field string) bool {
	_, ok := str._positions[field]
	return ok
}

func (str *View) Set(stru interface{}) (ss []Setter) {
	if structName(stru) != str._type {
		panic(fmt.Sprintf("wrong type: %T, needed %s or *%s", stru, str._type, str._type))
	}
	for field, ph := range str._positions {
		f := _Field(stru, field)
		// we need to handle the nil pointers differently,
		// since they may be handled via interfaces
		// and then they are not nil
		if f.Kind() == reflect.Ptr && f.IsNil() {
			ss = append(ss, ph.Set(nil))
			continue
		}
		ss = append(ss, ph.Set(f.Interface()))
	}
	return
}

func (str *View) scanPositions(stru interface{}, escaper Escaper) {
	str._positions = map[string]Position{}
	_EachRaw(stru,
		func(field reflect.StructField, v reflect.Value) {
			phName := fieldName(stru, field.Name, str._tag)
			ph := NewPosition(phName)
			if t := field.Tag.Get(str._tag); t != "" {
				if t != "-" { // "-" signals ignorance
					for _, escaperKey := range strings.Split(t, ",") {
						escFunc, ok := escaper[escaperKey]
						if !ok {
							panic("unknown escaper " + escaperKey + " needed by " + phName)
						}
						ph.Escaper = append(ph.Escaper, escFunc)
					}
					str._positions[field.Name] = ph
				}
				return
			}

			escFunc, ok := escaper[""]
			if !ok {
				panic(`missing empty escaper (key: "") needed by ` + phName)
			}
			ph.Escaper = append(ph.Escaper, escFunc)
			str._positions[field.Name] = ph
		})
	return
}

type Position struct {
	name, Value string
	Escaper     []func(interface{}) string
}

func NewPosition(name string, escaper ...func(interface{}) string) Position {
	return Position{name: name, Escaper: escaper}
}

func (p Position) Escape(escaper ...func(interface{}) string) Position {
	p.Escaper = escaper
	return p
}

func (p Position) Name() string { return p.name }

func (p Position) Set(val interface{}) Setter {
	var value string
	if len(p.Escaper) == 0 {
		value = fmt.Sprintf("%v", val)
	}

	for i, esc := range p.Escaper {
		if i == 0 {
			value = esc(val)
			continue
		}
		value = esc(value)
	}
	return Position{name: p.name, Value: value, Escaper: p.Escaper}
}

func (p Position) Setf(format string, vals ...interface{}) Setter {
	return p.Set(fmt.Sprintf(format, vals...))
}

func (p Position) WriteTo(w io.Writer) (int64, error) {
	i, err := w.Write([]byte(p.Value))
	return int64(i), err
}

type (
	stringer interface {
		String() string
	}

	writerto interface {
		WriteTo(io.Writer) (int64, error)
	}
)

type Template struct {
	*Buffer
	templ *places.Cache
}

func New(name string) *Template {
	t := &Template{}
	t.Buffer = newBuffer(name)
	return t
}

func (t *Template) New() *Buffer {
	return newBuffer(t.name)
}

func (t *Template) WriteSetter(p Setter) (err error) {
	_, err = t.Buffer.Write(places.DefaultStartDel)
	if err != nil {
		return
	}
	_, err = t.Buffer.WriteString(p.Name())
	if err != nil {
		return
	}
	_, err = t.Buffer.Write(places.DefaultEndDel)
	return
}

func (t *Template) MustWriteSetter(s Setter) {
	err := t.WriteSetter(s)
	if err != nil {
		panic(err.Error())
	}
}

func (t *Template) Add(data ...interface{}) (err error) {
	for _, d := range data {
		switch v := d.(type) {
		case Setter:
			err = t.WriteSetter(v)
		case []byte:
			_, err = t.Buffer.Write(v)
		case string:
			_, err = t.Buffer.WriteString(v)
		case byte:
			err = t.Buffer.WriteByte(v)
		case rune:
			_, err = t.Buffer.WriteRune(v)
		case writerto:
			_, err = v.WriteTo(t.Buffer)
		case stringer:
			_, err = t.Buffer.WriteString(v.String())
		default:
			_, err = t.Buffer.WriteString(fmt.Sprintf("%v", v))
		}
		if err != nil {
			return
		}
	}
	return
}

// add data to the template
func (t *Template) MustAdd(data ...interface{}) *Template {
	for _, d := range data {
		switch v := d.(type) {
		case Setter:
			t.MustWriteSetter(v)
		case []byte:
			t.MustWrite(v)
		case string:
			t.MustWriteString(v)
		case byte:
			t.MustWriteByte(v)
		case rune:
			t.MustWriteRune(v)
		case writerto:
			_, err := v.WriteTo(t.Buffer)
			if err != nil {
				panic(err.Error())
			}
		case stringer:
			t.MustWriteString(v.String())
		default:
			t.MustWriteString(fmt.Sprintf("%v", v))
		}
	}
	return t
}

func (t *Template) MustWrite(b []byte) {
	_, err := t.Buffer.Write(b)
	if err != nil {
		panic(err.Error())
	}
}

func (t *Template) MustWriteString(s string) {
	_, err := t.Buffer.WriteString(s)
	if err != nil {
		panic(err.Error())
	}
}

func (t *Template) MustWriteByte(b byte) {
	err := t.Buffer.WriteByte(b)
	if err != nil {
		panic(err.Error())
	}
}

func (t *Template) MustWriteRune(r rune) {
	_, err := t.Buffer.WriteRune(r)
	if err != nil {
		panic(err.Error())
	}
}

func (t *Template) MustWriteTo(w io.Writer) {
	_, err := t.Buffer.WriteTo(w)
	if err != nil {
		panic(err.Error())
	}
}

func (t *Template) Parse() error {
	t.templ = places.New(t.Buffer.Bytes())
	return nil
}

func (t *Template) MustParse() *Template {
	t.templ = places.New(t.Buffer.Bytes())
	return t
}

func mixedSetters(mixed ...interface{}) (ss []Setter) {
	for _, m := range mixed {
		switch v := m.(type) {
		case View:
			ss = append(ss, v.Set(v)...)
		case *View:
			ss = append(ss, v.Set(v)...)
		case Setter:
			ss = append(ss, v)
		case []Setter:
			ss = append(ss, v...)
		default:
			panic(fmt.Sprintf("unsupported type: %T, supported are: View, *View, Setter and []Setter", v))
		}
	}
	return
}

func (r *Template) ReplaceMixed(mixed ...interface{}) *Buffer {
	var bf bytes.Buffer
	r.ReplaceMixedTo(&bf, mixed...)
	return &Buffer{Buffer: &bf, name: r.Name()}
}

func (r *Template) ReplaceMixedTo(wr io.Writer, mixed ...interface{}) {
	ss := mixedSetters(mixed...)
	r.ReplaceTo(wr, ss...)
}

func (r *Template) ReplaceTo(wr io.Writer, setters ...Setter) {
	m := map[string]io.WriterTo{}
	for _, s := range setters {
		m[s.Name()] = s
	}

	r.templ.Replace(wr, places.WriterToMap(m))
}

func (r *Template) Replace(setters ...Setter) *Buffer {
	var bf bytes.Buffer
	r.ReplaceTo(&bf, setters...)
	return &Buffer{Buffer: &bf, name: r.Name()}
}

/*
// returns a fieldname as used in positions of structs
func FieldName(stru interface{}, field string) string {
	f := meta.Struct.Field(stru, field)
	r := fieldName(stru, field)
	if f.Interface() == nil {
		panic("field does not exist: " + r)
	}
	return r
}
*/

func fieldName(stru interface{}, field string, tag string) string {
	return strings.Replace(fmt.Sprintf("%T.%s#%s", stru, field, tag), "*", "", 1)
}
