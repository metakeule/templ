package benchmark

import (
	"fmt"
	"io"
	"text/template"
	"time"
)

type t struct {
	*template.Template
}

func NewTemplate() *t {
	return &t{}
}

func (xyyy *t) Parse(s string) (err error) {
	xyyy.Template, err = template.New(fmt.Sprintf("t-%v", time.Now().UnixNano())).Parse(s)
	if err != nil {
		panic(err.Error())
	}
	return
}

func (xyyy *t) Replace(data map[string]string, buf io.Writer) error {
	return xyyy.Template.Execute(buf, data)
}
