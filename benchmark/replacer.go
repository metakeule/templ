package benchmark

import (
	"strings"
)

type Naive2 struct {
	Replacements []string
	Template     string
}

func (xyyy *Naive2) Replace() (s string) {
	r := strings.NewReplacer(xyyy.Replacements...)
	return r.Replace(xyyy.Template)
}
