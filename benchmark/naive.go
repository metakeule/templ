package benchmark

import (
	"strings"
)

type Naive struct {
	Map      map[string]string
	Template string
}

func (xyyy *Naive) Replace() (s string) {
	s = xyyy.Template
	for k, v := range xyyy.Map {
		s = strings.Replace(s, k, v, -1)
	}
	return
}
