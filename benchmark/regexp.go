package benchmark

import (
	"regexp"
)

type Regexp struct {
	Regexp   *regexp.Regexp
	Map      map[string]string
	Replacer func(string) string
	Template string
}

func (xyyy *Regexp) Setup() {
	xyyy.Replacer = func(found string) string { return xyyy.Map[found] }
}

func (xyyy *Regexp) Replace() string {
	return xyyy.Regexp.ReplaceAllStringFunc(xyyy.Template, xyyy.Replacer)
}
