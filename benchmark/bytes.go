package benchmark

import (
	"bytes"
)

type Bytes struct {
	Map    map[string][]byte
	Buffer *bytes.Buffer
}

func (xyyy *Bytes) Parse(s string) {
	xyyy.Buffer = &bytes.Buffer{}
	xyyy.Buffer.WriteString(s)
}

func (xyyy *Bytes) Replace() (r []byte) {
	r = xyyy.Buffer.Bytes()

	for k, v := range xyyy.Map {
		r = bytes.Replace(r, []byte(k), v, -1)
	}
	return
}
