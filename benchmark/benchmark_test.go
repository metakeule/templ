package benchmark

/*
	TODO:

	make benchmarks against the template engine from go:

	- conditions (if)
	- iterations (for)
	- escaping (html)
	- embedding of templates
*/

import (
	"bytes"
	"fmt"
	"io"
	"regexp"
	"strings"
	"testing"

	// "gitlab.com/metakeule/replacer"
	"gitlab.com/metakeule/places"
	ttempl "gitlab.com/metakeule/templ"
)

var (
	StringT   = "a string with [@replacement1@] and [@replacement2@] that c@ntinues"
	TemplateT = "a string with {{.replacement1}} and {{.replacement2}} that c@ntinues"
	ByteT     = []byte(StringT)
	Expected  = "a string with repl1 and repl2 that c@ntinues"

	StringN   = ""
	TemplateN = ""
	ByteN     = []byte{}
	ExpectedN = ""

	StringM   = ""
	TemplateM = ""
	ByteM     = []byte{}
	ExpectedM = ""
)

var (
	Map = map[string]string{
		"[@replacement1@]": "repl1",
		"[@replacement2@]": "repl2",
	}

	Strings = []string{"[@replacement1@]", "repl1", "[@replacement2@]", "repl2"}

	StringMap = map[string]string{
		"replacement1": "repl1",
		"replacement2": "repl2",
	}

	ByteMap = map[string][]byte{
		"[@replacement1@]": []byte("repl1"),
		"[@replacement2@]": []byte("repl2"),
	}

	ByteMap2 = map[string][]byte{
		"replacement1": []byte("repl1"),
		"replacement2": []byte("repl2"),
	}

	Replacement1 = ttempl.NewPosition("replacement1")
	Replacement2 = ttempl.NewPosition("replacement2")

	MapM         = map[string]string{}
	StringMapM   = map[string]string{}
	ByteMapM     = map[string][]byte{}
	ByteMapM2    = map[string][]byte{}
	PositionMapM = map[*ttempl.Position]string{}
	StringsM     = []string{}
)

var (
	mapperNaive = &Naive{}
	naive2      = &Naive2{}
	mapperReg   = &Regexp{Regexp: regexp.MustCompile("(" + regexp.QuoteMeta("[") + "@[^@]+@" + regexp.QuoteMeta("]") + ")")}
	byts        = &Bytes{}
	//	repl        = replacer.New()
	templ = NewTemplate()
)

func PrepareM() {
	MapM = map[string]string{}
	ByteMapM = map[string][]byte{}
	ByteMapM2 = map[string][]byte{}
	StringMapM = map[string]string{}
	StringsM = []string{}
	PositionMapM = map[*ttempl.Position]string{}
	s := []string{}
	r := []string{}
	t := []string{}
	for i := 0; i < 5000; i++ {
		s = append(s, fmt.Sprintf(`a string with [@replacement%v@]`, i))
		t = append(t, fmt.Sprintf(`a string with {{.replacement%v}}`, i))
		r = append(r, fmt.Sprintf("a string with repl%v", i))
		key := fmt.Sprintf("replacement%v", i)
		val := fmt.Sprintf("repl%v", i)
		MapM["[@"+key+"@]"] = val
		ByteMapM["[@"+key+"@]"] = []byte(val)
		ByteMapM2[key] = []byte(val)
		StringMapM[key] = val
		StringsM = append(StringsM, "[@"+key+"@]", val)
		phk := ttempl.NewPosition(key)
		PositionMapM[&phk] = val
	}
	StringM = strings.Join(s, "")
	TemplateM = strings.Join(t, "")
	ExpectedM = strings.Join(r, "")
	ByteM = []byte(StringM)
}

func PrepareN() {
	s := []string{}
	r := []string{}
	t := []string{}
	for i := 0; i < 2500; i++ {
		s = append(s, StringT)
		r = append(r, Expected)
		t = append(t, TemplateT)
	}
	TemplateN = strings.Join(t, "")
	StringN = strings.Join(s, "")
	ExpectedN = strings.Join(r, "")
	ByteN = []byte(StringN)
}

func TestReplace(t *testing.T) {
	mapperNaive.Map = Map
	mapperNaive.Template = StringT
	if r := mapperNaive.Replace(); r != Expected {
		t.Errorf("unexpected result for %s: %#v", "mapperNaive", r)
	}

	naive2.Replacements = Strings
	naive2.Template = StringT
	if r := naive2.Replace(); r != Expected {
		t.Errorf("unexpected result for %s: %#v", "naive2", r)
	}

	mapperReg.Map = Map
	mapperReg.Template = StringT
	mapperReg.Setup()
	if r := mapperReg.Replace(); r != Expected {
		t.Errorf("unexpected result for %s: %#v", "mapperReg", r)
	}

	byts.Map = ByteMap
	byts.Parse(StringT)
	if r := byts.Replace(); string(r) != Expected {
		t.Errorf("unexpected result for %s: %#v, expected: %#v", "byts", string(r), Expected)
	}

	templ.Parse(TemplateT)
	var tbf bytes.Buffer
	if templ.Replace(StringMap, &tbf); tbf.String() != Expected {
		t.Errorf("unexpected result for %s: %#v, expected: %#v", "template", tbf.String(), Expected)
	}

	pl := places.New(ByteT)
	var bf bytes.Buffer
	if pl.Replace(&bf, places.StringMap(StringMap)); bf.String() != Expected {
		t.Errorf("unexpected result for %s: %#v", "places", bf.String())
	}

	tt := ttempl.New("t1").MustAdd(StringT).MustParse()

	if s := tt.Replace(Replacement1.Set("repl1"), Replacement2.Set("repl2")).String(); s != Expected {
		t.Errorf("unexpected result for %s: %#v", "replacer.Template", s)
	}
}

func TestReplaceN(t *testing.T) {
	PrepareN()
	mapperNaive.Map = Map
	mapperNaive.Template = StringN
	if r := mapperNaive.Replace(); r != ExpectedN {
		t.Errorf("unexpected result for %s: %#v", "mapperNaive", r)
	}

	naive2.Replacements = Strings
	naive2.Template = StringN
	if r := naive2.Replace(); r != ExpectedN {
		t.Errorf("unexpected result for %s: %#v", "naive2", r)
	}

	mapperReg.Map = Map
	mapperReg.Template = StringN
	mapperReg.Setup()
	if r := mapperReg.Replace(); r != ExpectedN {
		t.Errorf("unexpected result for %s: %#v", "mapperReg", r)
	}

	byts.Parse(StringN)
	byts.Map = ByteMap
	byts.Replace()
	if r := byts.Replace(); string(r) != ExpectedN {
		t.Errorf("unexpected result for %s: %#v, expected: %#v", "byts", string(r), ExpectedN)
	}

	templ.Parse(TemplateN)
	var tbf bytes.Buffer
	if templ.Replace(StringMap, &tbf); tbf.String() != ExpectedN {
		t.Errorf("unexpected result for %s: %#v, expected: %#v", "template", tbf.String(), ExpectedN)
	}

	pl := places.New(ByteN)
	var bf1 bytes.Buffer
	if pl.Replace(&bf1, places.StringMap(StringMap)); bf1.String() != ExpectedN {
		t.Errorf("unexpected result for %s: %#v", "places", bf1.String())
	}

	found := places.Find(ByteN)
	var bf2 bytes.Buffer
	if places.ReplaceBytes(ByteN, &bf2, found, ByteMap2); bf2.String() != ExpectedN {
		t.Errorf("unexpected result for %s: %v vs %v bytes", "places (bytes)", len(bf2.String()), len(ExpectedN))
	}

	tt := ttempl.New("t1").MustAdd(StringN).MustParse()

	if s := tt.Replace(Replacement1.Set("repl1"), Replacement2.Set("repl2")).String(); s != ExpectedN {
		t.Errorf("unexpected result for %s: %#v", "replacer.Template", s)
	}
}

func TestReplaceM(t *testing.T) {
	PrepareM()
	mapperNaive.Map = MapM
	mapperNaive.Template = StringM
	if r := mapperNaive.Replace(); r != ExpectedM {
		t.Errorf("unexpected result for %s: %#v", "mapperNaive", r)
	}

	naive2.Replacements = StringsM
	naive2.Template = StringM
	if r := naive2.Replace(); r != ExpectedM {
		t.Errorf("unexpected result for %s: %#v", "naive2", r)
	}

	mapperReg.Map = MapM
	mapperReg.Template = StringM
	mapperReg.Setup()
	if r := mapperReg.Replace(); r != ExpectedM {
		t.Errorf("unexpected result for %s: %#v", "mapperReg", r)
	}

	naive2.Replacements = StringsM
	naive2.Template = StringM
	if r := naive2.Replace(); r != ExpectedM {
		t.Errorf("unexpected result for %s: %#v", "naive2", r)
	}

	templ.Parse(TemplateM)
	var tbf bytes.Buffer
	if templ.Replace(StringMapM, &tbf); tbf.String() != ExpectedM {
		t.Errorf("unexpected result for %s: %#v, expected: %#v", "template", tbf.String(), ExpectedM)
	}

	pl := places.NewString(StringM)
	var bf bytes.Buffer
	if pl.Replace(&bf, places.StringMap(StringMapM)); bf.String() != ExpectedM {
		t.Errorf("unexpected result for %s: %#v", "places", bf.String())
	}

	found := places.Find(ByteM)
	var bf2 bytes.Buffer
	if places.ReplaceBytes(ByteM, &bf2, found, ByteMapM2); bf2.String() != ExpectedM {
		t.Errorf("unexpected result for %s: %v vs %v bytes", "places (bytes)", len(bf2.String()), len(ExpectedM))
	}

	tt := ttempl.New("t1").MustAdd(StringM).MustParse()

	setters := []ttempl.Setter{}

	for k, v := range PositionMapM {
		setters = append(setters, k.Set(v))
	}

	if s := tt.Replace(setters...).String(); s != ExpectedM {
		t.Errorf("unexpected result for %s: %#v", "replacer.Template", s)
	}
}

type noopwriter struct{}

func (noopwriter) Write(b []byte) (int, error) {
	return len(b), nil
}

var noop io.Writer = noopwriter{}

func BenchmarkNaive(b *testing.B) {
	b.StopTimer()
	mapperNaive.Map = Map
	mapperNaive.Template = StringN
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		mapperNaive.Replace()
	}
}

func BenchmarkNaive2(b *testing.B) {
	b.StopTimer()
	naive2.Replacements = Strings
	naive2.Template = StringN
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		naive2.Replace()
	}
}

func BenchmarkReg(b *testing.B) {
	b.StopTimer()
	mapperReg.Map = Map
	mapperReg.Template = StringN
	mapperReg.Setup()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		mapperReg.Replace()
	}
}

func BenchmarkByte(b *testing.B) {
	b.StopTimer()
	byts.Map = ByteMap
	byts.Parse(StringN)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		byts.Replace()
	}
}
func BenchmarkTemplate(b *testing.B) {
	b.StopTimer()
	templ.Parse(TemplateN)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		templ.Replace(StringMap, noop)
	}

}

func BenchmarkPlaces(b *testing.B) {
	b.StopTimer()
	cache := places.New(ByteN)
	mp := places.StringMap(StringMap)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		cache.Replace(noop, mp)
	}
}

func BenchmarkPlacesBytes(b *testing.B) {
	b.StopTimer()
	found := places.Find([]byte(StringN))
	var bf bytes.Buffer
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		bf.Reset()
		places.ReplaceBytes(ByteN, &bf, found, ByteMap2)
	}
	b.StopTimer()

	if bf.String() != ExpectedN {
		b.Errorf("unexpected result for places.ReplaceBytes")
	}
}

/*
func BenchmarkReplacer(b *testing.B) {
	b.StopTimer()
	repl.Parse(ByteN)
	var bf bytes.Buffer
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		repl.Replace(&bf, StringMap)
		bf.Reset()
	}
}
*/

func BenchmarkTempl(b *testing.B) {
	b.StopTimer()
	tt := ttempl.New("t1").MustAdd(StringN).MustParse()
	b.StartTimer()
	var bf bytes.Buffer
	for i := 0; i < b.N; i++ {
		tt.ReplaceTo(&bf, Replacement1.Set("repl1"), Replacement2.Set("repl2"))
	}
}

func BenchmarkNaiveM(b *testing.B) {
	b.StopTimer()
	PrepareM()
	mapperNaive.Map = MapM
	mapperNaive.Template = StringM
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		mapperNaive.Replace()
	}
}

func BenchmarkNaive2M(b *testing.B) {
	b.StopTimer()
	PrepareM()
	naive2.Replacements = StringsM
	naive2.Template = StringM
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		naive2.Replace()
	}
}

func BenchmarkRegM(b *testing.B) {
	b.StopTimer()
	PrepareM()
	mapperReg.Map = MapM
	mapperReg.Template = StringM
	mapperReg.Setup()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		mapperReg.Replace()
	}
}

func BenchmarkByteM(b *testing.B) {
	b.StopTimer()
	PrepareM()
	byts.Map = ByteMapM
	byts.Parse(StringM)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		byts.Replace()
	}
}

func BenchmarkTemplateM(b *testing.B) {
	b.StopTimer()
	PrepareM()
	templ.Parse(TemplateM)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		templ.Replace(StringMapM, noop)
	}
}

func BenchmarkPlacesM(b *testing.B) {
	b.StopTimer()
	PrepareM()
	cache := places.New(ByteM)
	mp := places.StringMap(StringMapM)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		cache.Replace(noop, mp)
	}
}

func BenchmarkPlacesMBytes(b *testing.B) {
	b.StopTimer()
	PrepareM()
	bt := []byte(StringM)
	found := places.Find(bt)
	var bf bytes.Buffer
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		bf.Reset()
		places.ReplaceBytes(bt, &bf, found, ByteMapM2)
	}

	b.StopTimer()
	if bf.String() != ExpectedM {
		b.Errorf("unexpected result for places.ReplaceBytes")
	}
}

/*
func BenchmarkReplacerM(b *testing.B) {
	b.StopTimer()
	PrepareM()
	repl.Parse(ByteM)
	var bf bytes.Buffer
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		repl.Replace(&bf, StringMapM)
		bf.Reset()
	}
}
*/

func BenchmarkTemplM(b *testing.B) {
	b.StopTimer()
	PrepareM()
	tt := ttempl.New("t1").MustAdd(StringM).MustParse()

	setters := []ttempl.Setter{}

	for k, v := range PositionMapM {
		setters = append(setters, k.Set(v))
	}

	b.StartTimer()
	//var bf bytes.Buffer
	for i := 0; i < b.N; i++ {
		tt.ReplaceTo(noop, setters...)
		//bf.Reset()
	}
}

func BenchmarkOnceNaive(b *testing.B) {
	for i := 0; i < b.N; i++ {
		mapperNaive.Map = Map
		mapperNaive.Template = StringN
		mapperNaive.Replace()
	}
}

func BenchmarkOnceNaive2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		naive2.Replacements = Strings
		naive2.Template = StringN
		naive2.Replace()
	}
}

func BenchmarkOnceReg(b *testing.B) {
	mapperReg.Setup()
	for i := 0; i < b.N; i++ {
		mapperReg.Map = Map
		mapperReg.Template = StringN
		mapperReg.Replace()
	}
}

func BenchmarkOnceByte(b *testing.B) {
	for i := 0; i < b.N; i++ {
		byts.Parse(StringN)
		byts.Map = ByteMap
		byts.Replace()
	}
}

func BenchmarkOnceTemplate(b *testing.B) {
	for i := 0; i < b.N; i++ {
		templ.Parse(TemplateN)
		templ.Replace(StringMap, noop)
	}
}

func BenchmarkOncePlaces(b *testing.B) {
	for i := 0; i < b.N; i++ {
		places.FindAndReplace(ByteN, noop, places.StringMap(StringMap))
	}
}

func BenchmarkOncePlacesBytes(b *testing.B) {
	for i := 0; i < b.N; i++ {
		found := places.Find(ByteN)
		places.ReplaceBytes(ByteN, noop, found, ByteMap2)
	}
}

/*
func BenchmarkOnceReplacer(b *testing.B) {
	for i := 0; i < b.N; i++ {
		repl.Parse(ByteN)
		var bf bytes.Buffer
		repl.Replace(&bf, StringMap)
	}
}
*/

func BenchmarkOnceTempl(b *testing.B) {
	for i := 0; i < b.N; i++ {
		tt := ttempl.New("t1").MustAdd(StringN).MustParse()
		//var bf bytes.Buffer
		tt.ReplaceTo(noop, Replacement1.Set("repl1"), Replacement2.Set("repl2"))
	}
}
