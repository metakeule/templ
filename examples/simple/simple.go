package main

import (
	"bytes"
	"fmt"
	"html"

	"gitlab.com/metakeule/templ"
)

func Html(name string) (t templ.Position) {
	return templ.NewPosition(name)
}

func Text(name string) (t templ.Position) {
	return templ.NewPosition(name, func(in interface{}) (out string) {
		return html.EscapeString(in.(string))
	})
}

var (
	person   = Text("person")
	greeting = Html("greeting")
	t        = templ.New("t")
)

func init() {
	t.MustAdd("<h1>Hi, ", person, "</h1>", greeting).MustParse()
}

func main() {
	fmt.Println(t.Replace(person.Set("S<o>meone"), greeting.Set("<div>Hi</div>")))

	var buffer bytes.Buffer
	for i := 0; i < 10; i++ {
		t.ReplaceTo(&buffer,
			person.Setf("Bugs <Bunny> %v", i+1),
			greeting.Set("<p>How are you?</p>\n"))
	}
	fmt.Println(buffer.String())
}
