package templ

import (
	"bytes"
	"testing"
)

var mytempl = struct {
	*Template
	FirstName Position
	LastName  Position
}{
	nil,
	NewPosition("firstname"),
	NewPosition("lastname"),
}

func init() {
	mytempl.Template = New("t").MustAdd("Hello, ", mytempl.FirstName, " ", mytempl.LastName, "!\n").MustParse()
}

var (
	firstname = NewPosition("firstname")
	lastname  = NewPosition("lastname")
	templ     = New("t").MustAdd("Hello, ", firstname, " ", lastname, "!\n").MustParse()
	expected  = "Hello, Donald Duck!\nHello, Mickey Mouse!\n"
)

func TestTemplate(t *testing.T) {
	var b bytes.Buffer
	templ.ReplaceTo(&b, firstname.Set("Donald"), lastname.Set("Duck"))
	templ.ReplaceTo(&b, firstname.Set("Mickey"), lastname.Set("Mouse"))

	if r := b.String(); r != expected {
		t.Errorf("Error in setting: expected\n\t%#v\ngot\n\t%#v\n", expected, r)
	}
}

func TestTemplate2(t *testing.T) {
	var b bytes.Buffer
	mytempl.ReplaceTo(&b, mytempl.FirstName.Set("Donald"), mytempl.LastName.Set("Duck"))
	mytempl.ReplaceTo(&b, mytempl.FirstName.Set("Mickey"), mytempl.LastName.Set("Mouse"))
	if r := b.String(); r != expected {
		t.Errorf("Error in setting: expected\n\t%#v\ngot\n\t%#v\n", expected, r)
	}
}
