package templ

import (
	"testing"
)

type li struct {
	*Template
}

type ul struct {
	*Template
	Fruit Position
	li    li
	bf    *Buffer
}

func (u ul) New() *ul {
	u.Fruit = NewPosition("fruit")
	u.li.Template = New("item").MustAdd("<li>", u.Fruit, "</li>").MustParse()
	u.Template = New("list").MustAdd("<ul>", u.li.Template, "</ul>").MustParse()
	u.bf = u.li.New()
	return &u
}

func (u *ul) Add(setters ...Setter) {
	u.li.ReplaceTo(u.bf, setters...)
}

func (u *ul) Finish() *Buffer {
	return u.Template.Replace(u.bf)
}

func TestSubTemplate2(t *testing.T) {
	u := ul{}.New()

	for _, fr := range []string{"Apple", "Pear"} {
		u.Add(u.Fruit.Set(fr))
	}

	r := u.Finish().String()

	if r != listOutput {
		t.Errorf("Error in setting: expected\n\t%#v\ngot\n\t%#v\n", listOutput, r)
	}
}

var listOutput = "<ul><li>Apple</li><li>Pear</li></ul>"

func TestSubTemplate(t *testing.T) {
	var (
		fruit = NewPosition("fruit")
		li    = New("item").MustAdd("<li>", fruit, "</li>").MustParse()
		ul    = New("list").MustAdd("<ul>", li, "</ul>").MustParse()
	)

	all := li.New()

	li.ReplaceTo(all, fruit.Set("Apple"))
	li.ReplaceTo(all, fruit.Set("Pear"))

	if r := ul.Replace(all).String(); r != listOutput {
		t.Errorf("Error in setting: expected\n\t%#v\ngot\n\t%#v\n", listOutput, r)
	}
}
